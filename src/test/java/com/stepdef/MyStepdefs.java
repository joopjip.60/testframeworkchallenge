package com.stepdef;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.messages.internal.com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.messages.internal.com.fasterxml.jackson.core.type.TypeReference;
import io.cucumber.messages.internal.com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import model.CategoryCount;
import org.junit.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class MyStepdefs {
    Logger logger = Logger.getLogger("MyStepDefs.class");
    Response response;
    ObjectMapper mapper = new ObjectMapper();
    private List<CategoryCount> expectedResult = new ArrayList<>();
    private List<CategoryCount> actualResult = new ArrayList<>();

    @Given("^I have expected result file \"([^\"]*)\"$")
    public void iHaveExpectedResultFile(String filePath) throws Throwable {
        String readExpected = readFromFile(filePath);
        expectedResult = mapper.readValue(readExpected, new TypeReference<List<CategoryCount>>() {
        });
    }

    @When("^I send get count category$")
    public void sendGetToCountEndpoint() throws JsonProcessingException {
        this.response = given()
                .baseUri("http://demoapp:8080")
                .when()
                .log()
                .all(true)
                .get("/api/count");
        actualResult = mapper.readValue(this.response.getBody().prettyPrint(), new TypeReference<List<CategoryCount>>() {
        });
    }

    private String readFromFile(String file) throws FileNotFoundException {
        File myObj = new File(file);
        Scanner myReader = new Scanner(myObj);
        String output = new String();
        while (myReader.hasNextLine()) {
            output = output.concat(myReader.nextLine());
        }
        return output;
    }

    @Then("the result is shown same as expected")
    public void theResultIsShownSameAsExpected() {
        Assert.assertEquals(expectedResult, actualResult);
    }
}
