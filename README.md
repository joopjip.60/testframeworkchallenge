# testFrameworkChallenge
This is a test for get Count document per Category. The test is written with Cucumber Framework. For stating the test container. 
Please make sure that the application is already start and the bridge between application and the test container is already created. 

If the application isn't started, pleas follow these steps: 
1. Clone the project from
   ```https://gitlab.com/joopjip.60/challenge```
2. Build Docker image
   ```docker build -t demo/myapp .```
3. Create the bridge between container 
   ```docker network create tulip-net```
4. Start the application with the connection bridge
   ```docker run --rm -p 8080:8080 --net tulip-net --name demoapp demo/myapp```

Now our application is started. Next we need to run the test container with the bridge that is connected to the application
1. Clone the project from
   ```https://gitlab.com/joopjip.60/challenge```
2. Build Docker image for test container
   ```docker  build -t testframework .```
3. Start test container with the connection bridge to the application
   ```docker run -it --net tulip-net --entrypoint /bin/bash testframework```
4. with the command --entrypoint /bin/bash, the bash shell in the container will be show.
5. we can execute the test with ```mvn test``` in the bash shell from test container
6. the test result will be in ```target/cucumber-reports```







