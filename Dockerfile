FROM maven:3.6.0-jdk-11-slim

WORKDIR /testframework
COPY . .
COPY /src/test/resources/featureFile /testframework/src/test/resources/featureFile
RUN mvn install -DskipTests=true
